// load the expressjs module into our application and save it in a variable called express.
const express =  require("express");

// localhost port number
const port = 4000;

// app is our server
// create an application that uses and stores it as app.
const app = express();

// middleware
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming json from our request
app.use(express.json());

// mockdata
let users = [
    {
        username: "TStark3000",
        email: "starkinustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "Thorthunder",
        email: 'lovethunder@mail.com',
        password: "iLoveStormBreaker"
    }
]

// express has methods to use as routes corresponding to HTTP methods
// Syntax:
    // app.method(<endpoint></endpoint>, function for request and response)

// [HTTP method GET]
    app.get("/", (request,response) => {
        // response.status = writeHead
        // response.send = write with end()
        response.send("Hello from express!")
    })

    // Mini Activity:
    app.get("/greeting", (request,response) => {

        response.status(201).send("Hello from Batch245-Man-Awit!")
    })

    app.get("/users", (request, response)=>{
        response.send(users);
    })

// [HTTP method POST]
    app.post("/users", (request, response)=>{
        let input = request.body;

        let newUser = {
            username: input.username,
            email: input.email,
            password: input.password
        }

        users.push(newUser)

        response.send(`The ${newUser.username} is now registered in our website with email: ${newUser.email}!`);
    })

// [HTTP method DELETE]
    app.delete("/users",(request, response)=>{
        let deletedUser = users.pop();
        response.send(deletedUser)
    })

// [HTTP method PUT]
    app.put("/users/:index", (request, response)=>{
        let indexToBeUpdated = request.params.index
        console.log(typeof indexToBeUpdated);

        indexToBeUpdated = parseInt(indexToBeUpdated);

        users[indexToBeUpdated].password = request.body.password

        response.send(users[indexToBeUpdated]);
    })



    app.listen(port, ()=> console.log("Server is running at port 4000"))
